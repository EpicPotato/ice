#THIS PROJECT IS BEING REWORKED
Go here for new version: https://github.com/icebit/icepixel

# ice
A top-down shooter about heat.

Your goal is to get as hot as you can.
If your temperature hits absolute zero, you are iced.

You can attack other particles by shooting at them.
If you hit them, you will take their heat.
If not, you will lose heat in the process.
