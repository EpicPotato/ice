//Player class

function Player(xPos, yPos) {
    Character.call(this, xPos, yPos);
    this.temp = 300;
    this.color = 'orange';
}

Player.prototype = Object.create(Character.prototype);

Player.prototype.input = function () {

    var dir = new Vec2(0, 0);

    if (keystate[left]) dir.x -= 1;
    if (keystate[right]) dir.x += 1;
    if (keystate[up]) dir.y -= 1;
    if (keystate[down]) dir.y += 1;
    return dir;
};

Player.prototype.iced = function () {
    gameState = 3;
}