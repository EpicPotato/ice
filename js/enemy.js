//Enemy class

function Enemy(xPos, yPos) {
    Character.call(this, xPos, yPos);
    this.temp = 300;
    this.color = 'blue';
}

Enemy.prototype = Object.create(Character.prototype);

Enemy.prototype.input = function () {
    var dir = new Vec2(0, 0);

    if (aiSetting == 0) {
        if (player.pos.x > this.pos.x) {
            dir.x += 1;
        }
        if (player.pos.x < this.pos.x) {
            dir.x -= 1;
        }
        if (player.pos.y > this.pos.y) {
            dir.y += 1;
        }
        if (player.pos.y < this.pos.y) {
            dir.y -= 1;
        }
    }

    if (aiSetting == 1) {
        if (player.pos.x > this.pos.x) {
            dir.x -= 1;
        }
        if (player.pos.x < this.pos.x) {
            dir.x += 1;
        }
        if (player.pos.y > this.pos.y) {
            dir.y -= 1;
        }
        if (player.pos.y < this.pos.y) {
            dir.y += 1;
        }
    }

    return dir;
}

Enemy.prototype.iced = function () {
    this.pos.x = 90;
    this.pos.y = 90;
    this.temp = 300;
}