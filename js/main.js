//Constants
var pi = Math.PI,
    tau = 2 * pi;

//Set up canvas and context
var ctx = document.getElementById('ctx').getContext('2d'),
    canvas = ctx.canvas;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
window.onresize = resize;

//Input setup
var keystate = {},
    left = 37,
    up = 38,
    right = 39,
    down = 40,
    space = 32,
    escape = 27;

var player = new Player(90, 90);
var enemy = new Enemy(100, 100);

//For scrolling
var camX = 500,
    camY = 200,
    worldWidth = 1000,
    worldHeight = 800,
    gridInterval = 100;

//Game states
//0 = Play
//1 = MainMenu -- initial value
//2 = About
//3 = Gameover
var gameState = 0;

//Debugging variables

//0 = Fight
//1 = Flea
//2 = None
var aiSetting = 0;

//Timers

//Global frame counter
var frameCount = 0;
var lastLoopTime;
var fps;
//Used for delta time compensasion
var optimalFps = 60;

//Gameover frame
var gameOverFrame = 0;

function load() {
    //Load assets here
    main();
}

function main() {
    //Initialize game
    init();

    //Main game loop
    var loop = function () {
        //Get delta time
        if (!lastLoopTime) {
            lastLoopTime = Date.now();
            fps = 0;
            loop();
            return;
        }
        var delta = (new Date().getTime() - lastLoopTime) / 1000;
        lastLoopTime = Date.now();
        fps = Math.round(1 / delta);

        update(delta);
        render();

        frameCount++;

        window.requestAnimationFrame(loop, canvas);
    }

    loop();
}

function init() {
    addEventListeners();
}

var pressTimer = 0;

function update(delta) {
    //Input

    //Debug keys

    //Increase/decrease heat

    if (keystate[69]) {
        if (keystate[space] && !keystate[16] && enemy.temp > 0) {
            enemy.temp += 2;
        } else {
            if (keystate[space] && keystate[16] && enemy.temp > 0) {
                enemy.temp -= 2;
            }
        }
    } else {
        if (keystate[space] && !keystate[16] && player.temp > 0) {
            player.temp += 2;
        } else {
            if (keystate[space] && keystate[16] && player.temp > 0) {
                player.temp -= 2;
            }
        }
    }



    //Increase world width
    if (keystate[73]) {
        if (worldWidth < 100000) worldWidth += 5;
    }

    //Decrease world width
    if (keystate[85]) {
        if (worldWidth > 100) worldWidth -= 5;
    }

    //Increase world height
    if (keystate[75]) {
        if (worldHeight < 100000) worldHeight += 5;
    }

    //Decrease world height
    if (keystate[74]) {
        if (worldHeight > 100) worldHeight -= 5;
    }


    if (pressTimer > 20) {
        if (keystate[66]) {
            if (aiSetting == 0) {
                aiSetting = 1;
            } else if (aiSetting == 1) {
                aiSetting = 2;
            } else if (aiSetting == 2) {
                aiSetting = 0;
            }
            pressTimer = 0;
        }
    } else {
        pressTimer++;
    }


    player.update(keystate, delta);
    enemy.update(keystate, delta);
    camX = player.pos.x - canvas.width / 2 + player.width;
    camY = player.pos.y - canvas.height / 2 + player.height;
}

function render() {


    //Clear screen
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    if (gameState == 0) {
        ctx.font = '50px Quicksand';
        ctx.fillStyle = 'white';
        ctx.textAlign = 'left';
        ctx.fillText(player.temp + ' K', 10, 50);

        //Draw grid

        drawGrid();

        ctx.globalAlpha = 0.7;

        //Test objects

        //drawTestObjects();

        ctx.globalAlpha = 1;

        player.render();
        enemy.render();

        debugRender();
    }

    if (gameState == 3) {
        player.render();

        if ((frameCount - gameOverFrame) < 120) {
            ctx.globalAlpha = (frameCount - gameOverFrame) / 120 * 0.2;
        } else {
            ctx.globalAlpha = 0.2;
        }

        ctx.fillStyle = 'blue';
        ctx.fillRect(0, 0, canvas.width, canvas.height);


        //Set shadow
        ctx.shadowColor = 'cyan';
        ctx.shadowBlur = Math.sin(frameCount / 60) * 30 + 40;

        drawGrid();

        if ((frameCount - gameOverFrame) < 120) {
            ctx.globalAlpha = (frameCount - gameOverFrame) / 120 * 1;
        } else {
            ctx.globalAlpha = 1;
        }

        ctx.font = '70px Quicksand';
        ctx.textAlign = 'center';
        ctx.fillStyle = 'cyan';
        ctx.fillText('iced', canvas.width / 2, canvas.height / 3);
    }


}

function resize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

function debugRender() {
    ctx.fillStyle = 'white';
    ctx.font = '12px Courier New';
    ctx.textAlign = 'right';
    ctx.fillText('Debug keys:', canvas.width - 20, 20);
    ctx.fillText('Space = Increase temp.', canvas.width - 20, 40);
    ctx.fillText('Shift + Space = Decrease temp.', canvas.width - 20, 60);

    ctx.fillText('B = Toggle AI setting', canvas.width - 20, 120);



    if (aiSetting == 0) {
        ctx.fillText('AI: Fight', canvas.width - 20, 140);
    } else if (aiSetting == 1) {
        ctx.fillText('AI: Flea', canvas.width - 20, 140);
    } else if (aiSetting == 2) {
        ctx.fillText('AI: None', canvas.width - 20, 140);

    }



    ctx.fillText('U = Increase world width, I = Decrease world width', canvas.width - 20, 170);
    ctx.fillText('J = Increase world height, K = Decrease world height', canvas.width - 20, 190);


    ctx.font = '20px Courier New';

    ctx.fillText('Width: ' + worldWidth + ', Height: ' + worldHeight, canvas.width - 20, 220);

    ctx.fillText('World Settings:', canvas.width - 20, 90);

    ctx.fillText('FPS: ' + fps, canvas.width - 20, canvas.height - 20);
}

function addEventListeners() {
    document.addEventListener('keydown', function (evt) {
        keystate[evt.keyCode] = true;
    });

    document.addEventListener('keyup', function (evt) {
        delete keystate[evt.keyCode];
    });
}

function drawGrid() {
    //Draw borders
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowColor = 'white';
    ctx.shadowBlur = 10;

    ctx.fillRect(0 - camX, 0 - camY, worldWidth, 2);
    ctx.fillRect(0 - camX, worldHeight - camY, worldWidth, 2);
    ctx.fillRect(0 - camX, 0 - camY, 2, worldHeight);
    ctx.fillRect(worldWidth - camX, 0 - camY, 2, worldHeight);

    //Draw grid

    ctx.globalAlpha = 0.3;
    ctx.fillStyle = 'white';
    ctx.shadowBlur = 8;

    for (i = 0; i < worldWidth / gridInterval; i++) {
        ctx.fillRect(i * gridInterval - camX, 0 - camY, 2, worldHeight);
    }

    for (i = 0; i < worldHeight / gridInterval; i++) {
        ctx.fillRect(0 - camX, i * gridInterval - camY, worldWidth, 2);
    }

    ctx.shadowBlur = 0;
    ctx.globalAlpha = 1;
}

function drawTestObjects() {
    ctx.fillRect(500 + (Math.sin(frameCount / 30) * 100) + 10 / 2 - camX, 90 + (Math.cos(frameCount / 30) * 100) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(900 - (Math.sin(frameCount / 30) * 100) + 10 / 2 - camX, 300 + (Math.cos(frameCount / 30) * 200) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(500 + (Math.sin(frameCount / 30) * 300) + 10 / 2 - camX, 90 + (Math.cos(frameCount / 30) * 50) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(900 - (Math.sin(frameCount / 30) * 50) + 10 / 2 - camX, 300 + (Math.cos(frameCount / 30) * 150) - 10 / 2 - camY, 10, 10);

    ctx.fillRect(200 + (Math.sin(frameCount / 30) * 100) + 10 / 2 - camX, 400 + (Math.cos(frameCount / 30) * 100) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(400 - (Math.sin(frameCount / 30) * 100) + 10 / 2 - camX, 700 + (Math.cos(frameCount / 30) * 200) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(100 + (Math.sin(frameCount / 30) * 300) + 10 / 2 - camX, 800 + (Math.cos(frameCount / 30) * 50) - 10 / 2 - camY, 10, 10);
    ctx.fillRect(300 - (Math.sin(frameCount / 30) * 50) + 10 / 2 - camX, 900 + (Math.cos(frameCount / 30) * 150) - 10 / 2 - camY, 10, 10);
}

window.onload = load;