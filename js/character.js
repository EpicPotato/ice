//Base character class

var Character = function (xPos, yPos) {
    this.pos = new Vec2(xPos, yPos);
    this.vel = new Vec2(0, 0);

    this.width = 70;
    this.height = 70;

    this.maxVel = 1.5;
    this.accel = 0.2;
    this.decel = 0.91;

    this.temp = 300;
    this.tempDecreaseTime = 6;
    this.timer = 0;
    this.color;
};

Character.prototype.update = function (keystate, delta) {
    //Decrease temperature

    this.timer++;

    if (this.temp <= 0) {
        this.iced();
    }

    //Update speed and acceleration based on heat

    this.maxVel = (0.04 + (0.04 * (this.temp / 4800))) * optimalFps * delta;
    this.accel = (0.001 + (0.01 * (this.temp / 9600))) * optimalFps * delta;

    var dir = this.input();

    this.vel.x += dir.x * this.accel;
    this.vel.y += dir.y * this.accel;

    if (this.timer > this.tempDecreaseTime && (dir.x != 0 || dir.y != 0)) {
        if (!this.temp <= 0) {
            this.temp -= 2;
            this.timer = 0;
        }
    }

    if (dir.x == 0) this.vel.x *= this.decel;
    if (dir.y == 0) this.vel.y *= this.decel;

    //Bounce off edges
    if (this.pos.x <= -this.width / 2) {
        this.vel.x = 0.03 * optimalFps * delta;
    }
    if (this.pos.x >= worldWidth - this.width - this.width / 2) {
        this.vel.x = -0.03 * optimalFps * delta;
    }
    if (this.pos.y <= this.height / 2) {
        this.vel.y = 0.03 * optimalFps * delta;
    }
    if (this.pos.y >= worldHeight - this.height / 2) {
        this.vel.y = -0.03 * optimalFps * delta;
    }

    this.pos.x += this.vel.x * this.temp;
    this.pos.y += this.vel.y * this.temp;


}

Character.prototype.render = function () {

    //Set color
    ctx.fillStyle = this.color;

    //Set shadow
    ctx.shadowColor = this.color;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;
    ctx.shadowBlur = this.temp / 600 * 80 * ((pi / 2 + Math.sin(frameCount / 40)) * 0.5 + 0.5) + 20;

    //ctx.beginPath();
    //ctx.arc(this.pos.x, this.pos.y, 40, tau, false);
    //ctx.fill();

    //Fill rectangle
    ctx.fillRect(this.pos.x + this.width / 2 - camX, this.pos.y - this.height / 2 - camY, this.width, this.height);

    //Draw temperature meter
    ctx.shadowBlur = 0;
    ctx.font = '20px Quicksand';
    ctx.textAlign = 'center';
    ctx.fillStyle = 'black';
    ctx.fillText(this.temp + ' K', this.pos.x + this.width - camX, this.pos.y + 7 - camY);

}

Character.prototype.input = function () {
    return new Vec2(0, 0);
}

Character.prototype.iced = function () {

}